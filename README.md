# lima

*lima* is a minimal, simple, text editor written in python.  It is based on
the code ideas for the *kilo* editor:

    https://viewsourcecode.org/snaptoken/kilo/index.html

The code is here:

    https://github.com/antirez/kilo
