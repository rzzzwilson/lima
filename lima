#!/usr/bin/env python3

"""
lima - a simple text editor in python.

Based loosely on the "kilo" editor by antirez:

    https://github.com/antirez/kilo
"""

import sys
import copy
import time
import fcntl
import struct
import atexit
import termios
import traceback
from enum import Enum

# start the logging system
try:
    import logger
    log = logger.Log('lima.log', logger.Log.DEBUG)
except ImportError:
    # if we don't have logger.py, don't crash
    # fake all log(), log.debug(), ... calls
    import time
    def logit(*args, **kwargs):
        pass
    log = logit
    log.debug = logit
    log.info = logit
    log.warn = logit
    log.error = logit
    log.critical = logit
    print('Module logger.py not available, logging turned off.')
    time.sleep(1)

# *** defines ***

# name and version of the editor
LimaName = 'lima'
LimaVersion = '0.0.1'

def CTRL_KEY(k):
    """Convert character to the CONTROL form."""

    return chr(ord(k) & 0x1f)

# names for various keys
BACKSPACE = 127

ARROW_LEFT = 1000
ARROW_RIGHT = 1001
ARROW_UP = 1002
ARROW_DOWN = 1003

DEL_KEY = 1004
HOME_KEY = 1005
END_KEY = 1006
PAGE_UP = 1007
PAGE_DOWN = 1008

BACK_TAB = 1009

SHIFT_DELETE = 1010
SHIFT_ARROW_UP = 1011
SHIFT_ARROW_DOWN = 1012
SHIFT_ARROW_RIGHT = 1013
SHIFT_ARROW_LEFT = 1014

CTRL_ARROW_UP = 1015
CTRL_ARROW_DOWN = 1016
CTRL_ARROW_RIGHT = 1017
CTRL_ARROW_LEFT = 1018

ESC = '\x1b'

BASE = 1020
ALT_A = BASE + 10
ALT_B = BASE + 11
ALT_C = BASE + 12
ALT_D = BASE + 13
ALT_E = BASE + 14
ALT_F = BASE + 15
ALT_G = BASE + 16
ALT_H = BASE + 17
ALT_I = BASE + 18
ALT_J = BASE + 19
ALT_K = BASE + 20
ALT_L = BASE + 21
ALT_M = BASE + 22
ALT_N = BASE + 23
ALT_O = BASE + 24
ALT_P = BASE + 25
ALT_Q = BASE + 26
ALT_R = BASE + 27
ALT_S = BASE + 28
ALT_T = BASE + 29
ALT_U = BASE + 30
ALT_V = BASE + 31
ALT_W = BASE + 32
ALT_X = BASE + 33
ALT_Y = BASE + 34
ALT_Z = BASE + 35

BASE += 30
ALT_a = BASE + 10
ALT_b = BASE + 11
ALT_c = BASE + 12
ALT_d = BASE + 13
ALT_e = BASE + 14
ALT_f = BASE + 15
ALT_g = BASE + 16
ALT_h = BASE + 17
ALT_i = BASE + 18
ALT_j = BASE + 19
ALT_k = BASE + 20
ALT_l = BASE + 21
ALT_m = BASE + 22
ALT_n = BASE + 23
ALT_o = BASE + 24
ALT_p = BASE + 25
ALT_q = BASE + 26
ALT_r = BASE + 27
ALT_s = BASE + 28
ALT_t = BASE + 29
ALT_u = BASE + 30
ALT_v = BASE + 31
ALT_w = BASE + 32
ALT_x = BASE + 33
ALT_y = BASE + 34
ALT_z = BASE + 35

# hard-coded tab stop
LIMA_TAB_STOP = 4

# number of times to see ^Q before quitting without saving
LIMA_QUIT_TIMES = 3

# number of times to ignore ^Q on dirty file before losing data
QuitTimes = LIMA_QUIT_TIMES

# *** data ***

# class for one row of text
class ERow:
    def __init__(self, chars):
        self.chars = chars
        self.render = None

    @property
    def size(self):
        return len(self.chars)

    @property
    def rsize(self):
        return len(self.render)

    def __str__(self):
        return f"ERoW(chars='{self.chars}', render='{self.render}')"

    def __repr__(self):
        return self.__str__()

# global object to hold the buffer environment
class Environment:
    def __str__(self):
        result = []
        for attrib in dir(self):
            if not attrib.startswith('_'):
                result.append(f'E.{attrib}={getattr(self, attrib)}')

        return '\n'.join(result)


E = Environment()

# *** terminal ***

def die(msg):
    """Handle error by printing message and stop."""

    disableRawMode()
    sys.stdout.write('\x1b[2J')
    sys.stdout.write('\x1b[H')
    print(msg)
    sys.exit(1)

def disableRawMode():
    """Turn off all settings, restore original mode."""

    termios.tcsetattr(sys.stdin, termios.TCSAFLUSH, E.orig_termios)

def enableRawMode():
    """Put terminal into the required "raw" mode."""

    E.orig_termios = termios.tcgetattr(sys.stdin)
    raw = termios.tcgetattr(sys.stdin)          # second copy that we mutate

    atexit.register(disableRawMode)

    raw[0] &= ~(termios.BRKINT | termios.ICRNL | termios.INPCK
                | termios.ISTRIP | termios.IXON)
    raw[1] &= ~(termios.OPOST)
    raw[3] |= termios.CS8
    raw[3] &= ~(termios.ECHO | termios.ICANON | termios.IEXTEN | termios.ISIG)
    raw[6][termios.VMIN] = b'\x00'      # 0
    raw[6][termios.VTIME] = b'\x01'     # 1
    termios.tcsetattr(sys.stdin, termios.TCSAFLUSH, raw)

## map the key following ESC[ to appropriate direction key
#esc1_keymap = {'A': ARROW_UP, 'B': ARROW_DOWN,
#               'C': ARROW_RIGHT, 'D': ARROW_LEFT,
#               'H': HOME_KEY, 'F': END_KEY, }
#
#esc2_keymap = {'1': HOME_KEY, '3': DEL_KEY,
#               '4': END_KEY, '5': PAGE_UP,
#               '6': PAGE_DOWN, '7': HOME_KEY,
#               '8': END_KEY, }
#
#esc3_keymap = {'H': HOME_KEY, 'F': END_KEY, }

# map the key following ESC[ to appropriate direction key
esc_keymap = {
              'A': ALT_A, 'B': ALT_B, 'C': ALT_C, 'D': ALT_D, 'E': ALT_D, 
              'F': ALT_F, 'G': ALT_G, 'H': ALT_H, 'I': ALT_I, 'J': ALT_J, 
              'K': ALT_K, 'L': ALT_L, 'M': ALT_M, 'N': ALT_N, 'O': ALT_O, 
              'P': ALT_P, 'Q': ALT_Q, 'R': ALT_R, 'S': ALT_S, 'T': ALT_T, 
              'U': ALT_U, 'V': ALT_V, 'W': ALT_W, 'X': ALT_X, 'Y': ALT_Y, 
              'Z': ALT_Z,

              'a': ALT_a,  'b': ALT_b, 'c': ALT_c, 'd': ALT_d, 'e': ALT_e,
              'f': ALT_f,  'g': ALT_g, 'h': ALT_h, 'i': ALT_i, 'j': ALT_j,
              'k': ALT_k,  'l': ALT_l, 'm': ALT_m, 'n': ALT_n, 'o': ALT_o,
              'p': ALT_p,  'q': ALT_q, 'r': ALT_r, 's': ALT_s, 't': ALT_t,
              'u': ALT_u,  'v': ALT_v, 'w': ALT_w, 'x': ALT_x, 'y': ALT_a,
              'z': ALT_z,

              '[A': ARROW_UP,
              '[B': ARROW_DOWN,
              '[C': ARROW_RIGHT,
              '[D': ARROW_LEFT,
              '[F': END_KEY,
              '[H': HOME_KEY,
              '[Z': BACK_TAB,

              '[3~': DEL_KEY,
              '[5~': PAGE_UP,
              '[6~': PAGE_DOWN,

              '[3;2~': SHIFT_DELETE,

              '[1;2A': SHIFT_ARROW_UP,
              '[1;2B': SHIFT_ARROW_DOWN,
              '[1;2C': SHIFT_ARROW_RIGHT,
              '[1;2D': SHIFT_ARROW_LEFT,

              '[1;5A': CTRL_ARROW_UP,
              '[1;5B': CTRL_ARROW_DOWN,
              '[1;5C': CTRL_ARROW_RIGHT,
              '[1;5D': CTRL_ARROW_LEFT,
             }

def editorReadKey():
    """Get and return the next editor keystroke."""

    c = ''
    while c == '':
        c = sys.stdin.read(1)

    if c == ESC:
        sequence = ''
        while sequence not in esc_keymap:
            sequence += sys.stdin.read(1)
            if len(sequence) > 7:
                print(f'{sequence=}')
                raise Exception
        log.critical(f'Returning {esc_keymap[sequence]}')
        return esc_keymap[sequence]

    if ord(c) == 127:
        log.critical("BS key changed to BACKSPACE")
        return BACKSPACE
#        return DEL_KEY

    log.critical(f"Simple key: {c} ({ord(c)})")
    return c

#def editorReadKeyX():
#    """Get and return the next editor keystroke."""
#
#    c = ''
#    while c == '':
#        c = sys.stdin.read(1)
#
#    #if c == '\x1b':
#    if c == ESC:
#        if not (first := sys.stdin.read(1)):
#            return ESC
#        if not (second := sys.stdin.read(1)):
#            return ESC
#
#        if first == '[':
#            if second in '0123456789':
#                if not (third := sys.stdin.read(1)):
#                    return '\x1b'
#                if third == '~':
#                    return esc2_keymap.get(second, '\x1b')
#            else:
#                esc = '\x1b'
#                return esc1_keymap.get(second, '\x1b')
#        elif first == 'O':
#            return esc3_keymap.get(second, '\x1b')
#
#        return '\x1b'
#
#    if ord(c) == 127:
#        return DEL_KEY
#
#    return c

def getWindowSize():
    """Return window dimensions.  Very different from kilo.c!"""

    s = struct.pack("HHHH", 0, 0, 0, 0)
    a = struct.unpack('hhhh', fcntl.ioctl(sys.stdout, termios.TIOCGWINSZ, s))
    return (a[0], a[1])

# *** row operations ***

def editorRowCxToRx(row, cx):
    """Calculate rx offset for the row text given cx."""

    rx = 0
    for ch in row.chars[:cx]:
        if ch == '\t':
            rx += (LIMA_TAB_STOP - 1) - (rx % LIMA_TAB_STOP)
        rx += 1

    return rx
def editorRowRxToCx(row, rx):
    """Convert a render index to a chars index."""

    cur_rx = 0
    for (cx, ch) in enumerate(row.chars):
        if ch == '\t':
            cur_rx += (LIMA_TAB_STOP - 1) - (cur_rx % LIMA_TAB_STOP)
        cur_rx += 1
        if cur_rx > rx:
            return cx
    return cx

def editorUpdateRow(row):
    """Update the 'render' stuff in a row."""

    new_render = []
    idx = 0
    for ch in row.chars:
        if ch =='\t':
            idx += 1
            new_render.append(' ')
            while idx % LIMA_TAB_STOP:
                idx += 1
                new_render.append(' ')
        else:
            new_render.append(ch)
            idx += 1

    row.render = ''.join(new_render)

def editorInsertRow(at, s):
    """Insert new row containing "s" at Y offset "at"."""

    if 0 <= at <= E.numrows:
        new_row = ERow(s)
        E.rows.insert(at, new_row)
        editorUpdateRow(new_row)
        E.numrows += 1
        E.dirty = True

def editorFreeRow(row):
    """Free memory in Kilo editor, does nothing here."""

    pass

def editorDelRow(at):
    """Delete row at "at" position."""

    if at <= 0 < E.numrows:
        del E.rows[at]
        E.numrows -= 1
        E.dirty = True

def editorRowInsertChar(row, at, c):
    """Insert character "ch" in "row" at line index "at"."""

    if 0 <= at <= row.size:
        row.chars = row.chars[:at] + c + row.chars[at:]
        editorUpdateRow(row)
        E.dirty = True

def editorRowAppendString(row, s):
    """Append string "s" to end of line in "row'."""

    row.chars += s
    editorUpdateRow(row)
    E.dirty = True

def editorRowDelChar(row, at):
    """Delete character at "at" position in "row"."""

    if 0 <= at < row.size:
        row.chars = row.chars[:at] + row.chars[at+1:]
        editorUpdateRow(row)
        E.dirty = True

# *** editor operations ***

def editorInsertChar(c):
    """Insert character at cursor position."""

    if E.cy == E.numrows:
        editorInsertRow(E.numrows, '')
    editorRowInsertChar(E.rows[E.cy], E.cx, c)
    E.cx += 1

def editorInsertNewline():
    """Insert NEWLINE at cursor position."""

    if E.cx == 0:
        editorInsertRow(E.cy, '')
    else:
        row = E.rows[E.cy]
        editorInsertRow(E.cy+1, row.chars[E.cx:])
        row.chars = row.chars[:E.cx]
        editorUpdateRow(row)
    E.cy += 1
    E.cx = 0

def editorDelChar():
    """Delete editor to the left of the cursor."""

    s = str(E)
    log.critical(f'editorDelChar:\n{s}')

    if E.cy == E.numrows:
        return
    if E.cx == 0 and E.cy == 0:
        return

    row = E.rows[E.cy]
    if E.cx > 0:
        editorRowDelChar(row, E.cx - 1)
        E.cx -= 1
    else:
        prev_row = E.rows[E.cy - 1]
        E.cx = prev_row.size
        editorRowAppendString(prev_row, row.chars)
        editorDelRow(E.cy)
        E.cy -= 1

# *** file i/o ***

def editorRowsToString():
    """Convert editor rows to a single string."""

    buf = []

    for j in E.rows:
        buf.append(j.chars)

    return '\n'.join(buf) + '\n'

def editorOpen(filename):
    """Open file to edit."""

    E.filename = filename
    with open(filename) as fp:
        for line in fp:
            line = line.rstrip('\r')
            line = line.rstrip('\n')
            editorInsertRow(E.numrows, line)

    E.dirty = False

def editorSave():
    """Save text back to the file."""

    if E.filename is None:
        E.filename = editorPrompt('Save as: (ESC to cancel)')
        if not E.filename:
            editorSetStatusMessage('Save aborted');
            return

    buf = editorRowsToString()

    with open(E.filename, "w") as fd:
        fd.write(buf)
    E.dirty = False
    editorSetStatusMessage(f"{len(buf)} bytes written to file '{E.filename}'")

# *** find ***

last_match = -1     # row index last match occurred on
direction = 1       # direction of the search, 1 forward, -1 backwards

def editorFindCallback(query, key):
    """Callback function for incremental search."""

    last_match = -1
    direction = 1

    if key == '\r' or key == '\x1b':
        last_match = -1
        direction = 1
        return
    elif key in {ARROW_RIGHT, ARROW_DOWN}:
        direction = 1
    elif key in {ARROW_LEFT, ARROW_UP}:
        direction = -1
    else:
        last_match = -1
        direction = 1

    if last_match == -1:
        direction = 1
    current = last_match

    for (i, row) in enumerate(E.rows):
        current += direction
        if current == -1:
            current = E.numrows - 1
        elif current == E.numrows:
            current = 0

        row = E.rows[current]
        match = row.render.find(query)
        if match != -1:
            last_match = current
            E.cy = current
            E.cx = editorRowRxToCx(row, match)
            E.rowoff = E.numrows
            break

def editorFind():
    """A simple "find" operation."""

    # save current position in case we cancel search
    saved_cx = E.cx
    saved_cy = E.cy
    saved_coloff = E.coloff
    saved_rowoff = E.rowoff

    # do the incremental search
    query = editorPrompt("Search: (Use ESC/Arrows/Enter): ", editorFindCallback)

    if query is None:
        # search cancelled, restore original state
        E.cx = saved_cx
        E.cy = saved_cy
        E.coloff = saved_coloff
        E.rowoff = saved_rowoff

# *** append buffer ***

# *** output ***

def editorScroll():
    """Scroll the display (if possible) when at top/bottom limits."""

    E.rx = 0
    if E.cy < E.numrows:
        E.rx = editorRowCxToRx(E.rows[E.cy], E.cx)

    if E.cy < E.rowoff:
        E.rowoff = E.cy
    if E.cy >= E.rowoff + E.screenrows:
        E.rowoff = E.cy - E.screenrows + 1
    if E.rx < E.coloff:
        E.coloff = E.rx
    if E.rx >= E.coloff + E.screencols:
        E.coloff = E.rx - E.screencols + 1

def editorDrawRows(ab):
    """Draw the rows of data on the editor screen."""

    for y in range(E.screenrows):
        filerow = y + E.rowoff
        if filerow >= E.numrows:
#            if E.numrows == 0 and y == E.screenrows // 3:
            if y == E.screenrows // 3:
                welcome = f'{LimaName} editor -- version {LimaVersion}'
                if len(welcome) > E.screencols:
                    welcome = welcome[:E.screencols]
                padding = (E.screencols - len(welcome)) // 2
                if padding:
                    ab.append('~')
                    padding -= 1
                ab.append(' '*padding + welcome)
            else:
                ab.append('~')
        else:
            xlen = E.rows[filerow].rsize - E.coloff
            if xlen < 0:
                xlen = 0
            if xlen > E.screencols:
                xlen = E.screencols
            row = E.rows[filerow]
            if row.size > E.screencols:
                xlen = E.screencols
            ab.append(row.render[E.coloff:E.coloff+xlen])

        ab.append('\x1b[K')
        ab.append('\r\n')

def editorDrawStatusBar(ab):
    """Draw the status bar."""

    ab.append('\x1b[7m')
    filename = E.filename if E.filename else '[No Name]'
    ab.append(filename)
    ab.append('*' if E.dirty else ' ')
    rstatus = f'{E.cy+1}/{E.numrows}'
    ab.append(' ' * (E.screencols - len(filename) - len(rstatus) - 1))
    ab.append(rstatus)
    ab.append('\x1b[m')
    ab.append('\r\n')

def editorDrawMessageBar(ab):
    """Draw a message string in the message line."""

    ab.append('\x1b[K')
    msg = E.statusmsg
    if len(msg) > E.screencols:
        msg = msg[:E.screencols]
    if len(msg) and (time.time() - E.statusmsg_time) < 5:
        ab.append(msg)

def editorRefreshScreen():
    """Refresh the screen."""

    editorScroll()

    ab = []
    ab.append('\x1b[?25l')
    ab.append('\x1b[H')

    editorDrawRows(ab)
    editorDrawStatusBar(ab)
    editorDrawMessageBar(ab)

    ab.append(f'\x1b[{(E.cy-E.rowoff)+1};{(E.rx-E.coloff)+1}H')
    ab.append('\x1b[?25h')
    sys.stdout.write(''.join(ab))

def editorSetStatusMessage(msg):
    """Display status message."""

    E.statusmsg = msg
    E.statusmsg_time = time.time()

# *** input ***

def editorPrompt(prompt, callback=None):
    """Prompt user about something."""

    buff = []

    while True:
        editorSetStatusMessage(f"{prompt} {''.join(buff)}")
        editorRefreshScreen()

        c = editorReadKey()

        if c in {DEL_KEY, CTRL_KEY('h'), BACKSPACE}:
            if buff:
                del buff[-1]
        elif c == '\x1b':
            editorSetStatusMessage("")
            if callback:
                callback(''.join(buff), c)
            return None
        elif c == '\r':
            if buff:
                editorSetStatusMessage('')
                if callback:
                    callback(''.join(buff), c)
                return ''.join(buff)
        elif not isinstance(c, int) and c.isprintable() and ord(c) < 128:
            buff.append(c)

        if callback:
            callback(''.join(buff), c)

def editorMoveCursor(key):
    """Update the cursor position after a move."""

    if key == ARROW_LEFT:
        if E.cx > 0:
            E.cx -= 1
        elif E.cy > 0:
            E.cy -= 1
            E.cx = E.rows[E.cy].size

    elif key == ARROW_RIGHT:
        if E.cy < E.numrows:    # if not on last, can move RIGHT
            if E.cx < E.rows[E.cy].size:
                E.cx += 1
            elif E.cx == E.rows[E.cy].size:
                E.cy += 1
                E.cx = 0
    elif key == ARROW_UP:
        if E.cy > 0:
            E.cy -= 1
    elif key == ARROW_DOWN:
        if E.cy < E.numrows:
            E.cy += 1

    rowlen = 0 if E.cy >= E.numrows else E.rows[E.cy].size
    if E.cx > rowlen:
        E.cx = rowlen

def editorProcessKeypress():
    """Perform operation for the key."""

    global QuitTimes

    c = editorReadKey()

    if c == '\r':
        editorInsertNewline()
    elif c == CTRL_KEY('q'):
        if E.dirty and QuitTimes:
            editorSetStatusMessage("WARNING!!! File has unsaved changes. "
                                   f"Press Ctrl-Q {QuitTimes} more times to quit.")
            QuitTimes -= 1
            return
        sys.stdout.write('\x1b[2J')     # clear the screen
        sys.stdout.write('\x1b[H')
        sys.exit(0)                     # and quit
    elif c == CTRL_KEY('s'):
        editorSave()
    elif c == HOME_KEY:
        E.cx = 0
    elif c == END_KEY:
        if E.cy < E.numrows:
            E.cx = E.rows[E.cy].size
    elif c == CTRL_KEY('f'):
        editorFind()
    elif c in {BACKSPACE, CTRL_KEY('h'), DEL_KEY}:
        log.critical(f'editorProcessKeypress:{c=}')
        if c == DEL_KEY:
            editorMoveCursor(ARROW_RIGHT)
        editorDelChar()
    elif c in {PAGE_UP, PAGE_DOWN}:
        if c == PAGE_UP:
            E.cy = E.rowoff
        else:
            E.cy = E.rowoff + E.screenrows - 1
            if E.cy > E.numrows:
                E.cy = E.numrows

        for _ in range(E.screenrows):
            editorMoveCursor(ARROW_UP if c == PAGE_UP else ARROW_DOWN)
    elif c in {ARROW_UP, ARROW_DOWN, ARROW_LEFT, ARROW_RIGHT}:
        editorMoveCursor(c)
    elif c in {CTRL_KEY('l'), '\x1b'}:
        pass
    else:
        editorInsertChar(c)

    QuitTimes = LIMA_QUIT_TIMES

# *** init ***

def initEditor():
    """Initialize the editor."""

    E.cx = 0
    E.cy = 0
    E.rx = 0
    E.rowoff = 0
    E.coloff = 0
    E.rows = []
    E.numrows = 0
    E.dirty = False
    E.filename = None
    E.statusmsg = None
    E.statusmsg_time = 0

    (E.screenrows, E.screencols) = getWindowSize()
    E.screenrows -= 2       # make room for the status and message lines

def main():
#    enableRawMode()
#    while True:
#        c = sys.stdin.read(1)
#        if c == 'q':
#            break
#        if not c:
#            continue
#        if not c.isprintable():
#            print(f"{ord(c)}", end='\r\n')
#        else:
#            print(f"{ord(c)} ({c})", end='\r\n');

    """Main processing loop for the editor."""

    enableRawMode()
    initEditor()
    if len(sys.argv) >= 2:
        try:
            editorOpen(sys.argv[1])
        except FileNotFoundError:
            print(f"File '{sys.argv[1]}' not found.", end='')
            sys.stdout.flush()
            time.sleep(1)

    editorSetStatusMessage('HELP: Ctrl-S = save | Ctrl-Q = quit | Ctrl-F = find')

    while True:
        editorRefreshScreen()
        editorProcessKeypress()

    return 0

################## start of mainline code #####################################

# handler for uncaught exceptions
def excepthook(type, value, tb):
    """Handler for uncaught exceptions."""

    disableRawMode()
    sys.stdout.write('\x1b[2J')     # clear the screen
    sys.stdout.write('\x1b[H')

    msg = '\n' + '=' * 80
    msg += '\nUncaught exception:\n'
    msg += ''.join(traceback.format_exception(type, value, tb))
    msg += '=' * 80 + '\n'
    print(msg)

    sys.exit(1)

# plug our uncaught exception handler into the python system
sys.excepthook = excepthook

# start the editor
sys.exit(main())
